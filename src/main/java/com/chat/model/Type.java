package com.chat.model;

public enum Type {
    CHAT, JOIN, LEAVE
}
