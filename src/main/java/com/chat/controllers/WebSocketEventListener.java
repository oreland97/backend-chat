package com.chat.controllers;

import com.chat.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import static com.chat.model.Type.LEAVE;

@Component
public class WebSocketEventListener {

    private SimpMessageSendingOperations messageSendingOperations;

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);

    public WebSocketEventListener(SimpMessageSendingOperations messageSendingOperations) {
        this.messageSendingOperations = messageSendingOperations;
    }

    @EventListener
    public void handleWebSocketConnectionListener(SessionConnectedEvent event) {
        LOGGER.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectionListener(SessionDisconnectEvent event) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) accessor.getSessionAttributes().get("username");

        if (username != null) {
            LOGGER.info("User disconnected:" + username);

            Message message = new Message();
            message.setType(LEAVE);
            message.setSender(username);

            messageSendingOperations.convertAndSend("/topic/public", message);
        }
    }

    
}